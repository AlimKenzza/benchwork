package main

import (
	"fmt"
	"gitlab.com/AlimKenzza/hw3_bench/easyJson"
	"io"
	"io/ioutil"
	"strings"
)

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	fileContents, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}
	seenBrowsers := make(map[string]interface{})
	lines := strings.Split(string(fileContents), "\n")
	user := easyJson.User{}
	fmt.Fprintln(out, "found users:")
	for i, line := range lines {
		err := user.UnmarshalJSON([]byte(line))
		if err != nil {
		panic(err)
	}
		isAndroid := false
		isMSIE := false

		browsers := user.Browsers
		for _, browserRaw := range browsers {
			browser := browserRaw

			switch {
			case strings.Contains(browser, "Android"):
				isAndroid = true
			case strings.Contains(browser, "MSIE"):
				isMSIE = true
			default:
				continue
			}

			seenBrowsers[browser] = true
		}
		if !(isAndroid && isMSIE) {
			continue
		}
		email := strings.Replace(user.Email, "@", " [at] ", 1)
		fmt.Fprintf(out, "[%d] %s <%s>\n", i, user.Name, email)
	}
	fmt.Fprintf(out, "\n")
	fmt.Fprintln(out, "Total unique browsers", len(seenBrowsers))
}